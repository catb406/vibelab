import Foundation

// Создать класс родитель и 2 класса наследника

class Animal {
    var name: String
    var sound: String
    
    init(name: String, sound: String) {
        self.name = name
        self.sound = sound
    }
    
    func makeSound() {
        print("\(name) says \(sound)")
    }
}

class Cat: Animal {
    override init(name: String, sound: String) {
        super.init(name: name, sound: sound)
    }
    
    override func makeSound() {
        print("\(name) meows \(sound)")
    }
}

class Dog: Animal {
    override init(name: String, sound: String) {
        super.init(name: name, sound: sound)
    }
    
    override func makeSound() {
        print("\(name) barks \(sound)")
    }
}

let dog = Dog(name: "nancy", sound: "aw")
dog.makeSound()

// Создать класс House

class House {
    var width: Double
    var height: Double
    
    init(width: Double, height: Double) {
        self.width = width
        self.height = height
    }
    
    func create() {
        let area = width * height
        print("The house has been created. Its area is \(area).")
    }
    
    func destroy() {
        print("The house has been destroyed.")
    }
}

let house = House(width: 10, height: 15)
house.create()
house.destroy()

// Создайте класс с методами, которые сортируют массив учеников по разным параметрам

class Students {
    struct Student {
        var name: String
        var age: Int
        var grade: Int
    }
    
    var list: [Student] = []
    
    func sortByName() {
        list.sort { $0.name < $1.name }
    }
    
    func sortByAge() -> [Student] {
        return list.sorted { $0.age < $1.age }
    }
    
    func sortByGrade() {
        list.sort { $0.grade > $1.grade }
    }
}

let students = Students()
students.list = [Students.Student(name: "Bob", age: 20, grade: 85),
                 Students.Student(name: "Alice", age: 18, grade: 90),
                 Students.Student(name: "Charlie", age: 22, grade: 80)]

students.sortByName()
print("Sorted by name:")
for student in students.list {
    print(student.name)
}

students.sortByAge()
print("Sorted by age:")
for student in students.list {
    print(student.name)
}

students.sortByGrade()
print("Sorted by grade:")
for student in students.list {
    print(student.name)
}

// структуры передаются по значению, классы по ссылке

// структура
struct Kitty {
    var name: String
    
    func call() {
        print("hello \(name)")
    }

}

let myCat = Kitty(name: "Kitty")
myCat.call() // "hello Kitty"

// класс
class Person {
    var name: String
    var age: Int
    
    init(name: String, age: Int) {
        self.name = name
        self.age = age
    }
    
    func celebrateBirthday() {
        self.age += 1
    }
    
    func tellAge(){
        print("\(name) is \(age) years old")
    }
}

let jake = Person(name:"Jake", age: 10)
jake.tellAge()
jake.celebrateBirthday()
jake.tellAge()



enum Suit: String, CaseIterable {
    case hearts = "черви"
    case diamonds = "бубны"
    case clubs = "трефы"
    case spades = "пики"
}

enum Rank: Int, CaseIterable {
    case two = 2, three, four, five, six, seven, eight, nine, ten, jack, queen, king, ace

    var description: String {
        switch self {
        case .jack: return "валет"
        case .queen: return "дама"
        case .king: return "король"
        case .ace: return "туз"
        default: return "\(rawValue)"
        }
    }
}

struct Card: CustomStringConvertible, Hashable {
    let suit: Suit
    let rank: Rank

    var description: String {
        return "\(rank.description) \(suit.rawValue)"
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(suit)
        hasher.combine(rank)
    }

    static func ==(lhs: Card, rhs: Card) -> Bool {
        return lhs.suit == rhs.suit && lhs.rank == rhs.rank
    }
}

func generateRandomHand() -> [Card] {
    var hand: [Card] = []
    var uniqueCards = Set<Card>()

    while uniqueCards.count < 5 {
        let randomSuit = Suit.allCases.randomElement()!
        let randomRank = Rank.allCases.randomElement()!
        let card = Card(suit: randomSuit, rank: randomRank)

        if !uniqueCards.contains(card) {
            uniqueCards.insert(card)
            hand.append(card)
        }
    }

    return hand
}

func analyzeHand(_ hand: [Card]) -> String {
    let sortedHand = hand.sorted { $0.rank.rawValue < $1.rank.rawValue }
    var suitsCount: [Suit: Int] = [:]
    var ranksCount: [Rank: Int] = [:]

    for card in sortedHand {
        suitsCount[card.suit, default: 0] += 1
        ranksCount[card.rank, default: 0] += 1
    }

    let isFlush = suitsCount.values.contains(5)
    var isStraight = false
    var highestRankInStraight: Rank?

    for (index, card) in sortedHand.enumerated() {
        if index < sortedHand.count - 1 && sortedHand[index + 1].rank.rawValue - card.rank.rawValue == 1 {
            if highestRankInStraight == nil {
                highestRankInStraight = card.rank
            }
        } else {
            highestRankInStraight = nil
        }

        if highestRankInStraight != nil && sortedHand[index + 1].rank.rawValue - highestRankInStraight!.rawValue == 4 {
            isStraight = true
            break
        }
    }

    if isFlush && isStraight {
        return "У вас \(sortedHand[0].suit.rawValue) стрит флеш"
    }

    let rankOccurrences = Array(ranksCount.values)
    let fourOfAKind = rankOccurrences.contains(4)
    let threeOfAKind = rankOccurrences.contains(3)
    let pairsCount = rankOccurrences.filter { $0 == 2 }.count

    if fourOfAKind {
        let fourOfAKindRank = ranksCount.first { $0.value == 4 }!.key.description
        return "У вас каре из \(fourOfAKindRank)"
    }

    if threeOfAKind && pairsCount == 1 {
        let threeOfAKindRank = ranksCount.first { $0.value == 3 }!.key.description
        let pairRank = ranksCount.first { $0.value == 2 }!.key.description
        return "У вас фулл-хаус: \(threeOfAKindRank) и \(pairRank)"
    }

    if isFlush {
        return "У вас флеш (\(sortedHand[0].suit.rawValue))"
    }

    if isStraight {
        return "У вас стрит, заканчивающийся на \(highestRankInStraight!.description)"
    }

    if threeOfAKind {
        let threeOfAKindRank = ranksCount.first { $0.value == 3 }!.key.description
        return "У вас тройка из \(threeOfAKindRank)"
    }

    if pairsCount == 2 {
        let pairRanks = ranksCount.filter { $0.value == 2 }.map { $0.key.description }
        return "У вас две пары: \(pairRanks[0]) и \(pairRanks[1])"
    }

    if pairsCount == 1 {
        let pairRank = ranksCount.first { $0.value == 2 }!.key.description
        return "У вас пара из \(pairRank)"
    }

    let highestCard = sortedHand.last!.description
    return "У вас старшая карта: \(highestCard)"
}


let hand = generateRandomHand()
print("Ваша рука: \(hand)")
let result = analyzeHand(hand)
print(result)

final class Box<T> {
    var value: T
    init(_ value: T) {
        self.value = value
    }
}

struct IOSCollection {
    private var storage: Box<[String]> = Box([])

    var videos: [String] {
        get { return storage.value }
        set {
            if !isKnownUniquelyReferenced(&storage) {
                storage = Box(storage.value)
            }
            storage.value = newValue
        }
    }
}

// Создание протокола Hotel и класса HotelAlfa

protocol Hotel {
    init(roomCount: Int)
}

class HotelAlfa: Hotel {
    var roomCount: Int

    required init(roomCount: Int) {
        self.roomCount = roomCount
    }
}
// Создание протокола GameDice и расширения для Int
protocol GameDice {
    var numberDice: Int { get }
}

extension Int: GameDice {
    var numberDice: Int {
        print("Выпало \(self) на кубике")
        return self
    }
}

let diceCoub = 4
let number = diceCoub.numberDice

// Создание протокола с необязательным свойством и класса, реализующего этот протокол
protocol SomeProtocol {
    var requiredProperty: String { get set }
    var optionalProperty: String? { get set }
}

class SomeClass: SomeProtocol {
    var optionalProperty: String?
    
    var requiredProperty: String

    init(requiredProperty: String) {
        self.requiredProperty = requiredProperty
    }
}

// Создание двух протоколов и класса Company
enum Platform {
    case iOS, android, web
}

protocol Coding {
    var time: Int { get }
    var codeAmount: Int { get }
    func writeCode(platform: Platform, numberOfSpecialists: Int)
}

protocol StoppingCoding {
    func stopCoding()
}

class Company: Coding, StoppingCoding {
    var programmersCount: Int
    var specializations: [Platform]

    var time: Int = 0
    var codeAmount: Int = 0

    init(programmersCount: Int, specializations: [Platform]) {
        self.programmersCount = programmersCount
        self.specializations = specializations
    }
   
        func writeCode(platform: Platform, numberOfSpecialists: Int) {
            guard numberOfSpecialists <= programmersCount else {
                print("Недостаточно программистов для работы с платформой \(platform).")
                return
            }

            switch platform {
            case .iOS:
                print("Разработка началась. Пишем код для iOS.")
            case .android:
                print("Разработка началась. Пишем код для Android.")
            case .web:
                print("Разработка началась. Пишем код для веб-платформы.")
            }

            time += 10
            codeAmount += 1000
        }

        func stopCoding() {
            print("Работа закончена. Сдаю в тестирование.")
        }
    }

    let myCompany = Company(programmersCount: 5, specializations: [.iOS, .android, .web])

    myCompany.writeCode(platform: .iOS, numberOfSpecialists: 3)
    myCompany.writeCode(platform: .android, numberOfSpecialists: 6)
    myCompany.stopCoding()

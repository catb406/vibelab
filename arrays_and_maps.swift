// Отсортировать массив в одну сторону
let unsortedArray = [3, 1, 4, 1, 5, 9, 2, 6, 5]
let sortedArray = unsortedArray.sorted { $0 < $1 }
print(sortedArray)

// Отсортировать массив в обратную сторону
let reverseSortedArray = unsortedArray.sorted { $0 > $1 }
print(reverseSortedArray)

// Метод для сортировки массива по количеству букв в имени
func sortFriendsByLength(_ names: [String]) -> [String] {
    return names.sorted { $0.count < $1.count }
}

// Отсортировать массив имен по количеству букв в имени
let friends = ["Василий", "Петя", "Оля", "Николай", "Ян"]
let sortedFriends = sortFriendsByLength(friends)
print(sortedFriends)

// Создаем словарь, где ключ - кол-во символов в имени, а в значении - массив имен друзей с таким количеством символов
var friendsByLength: [Int: [String]] = [:]
for friend in sortedFriends {
    let count = friend.count
    if var friends = friendsByLength[count] {
        // Добавляем имя друга в существующий массив, если такой ключ уже есть
        friends.append(friend)
        friendsByLength[count] = friends
    } else {
        // Создаем новый массив имен для нового ключа
        friendsByLength[count] = [friend]
    }
}
print(friendsByLength)


// Функция для вывода значения из словаря по ключу
func printFriendByLength(_ length: Int, from dictionary: [Int: [String]]) {
    if let friend = dictionary[length] {
        print("Friend with length \(length): \(friend)")
    } else {
        print("No friend with length \(length) found.")
    }
}

// Вывести имя из словаря по ключу
printFriendByLength(3, from: friendsByLength)

// Функция для проверки двух массивов на пустоту и вывода в консоль
func checkAndPrintArrays(_ stringArray: inout [String], _ intArray: inout [Int]) {
    if stringArray.isEmpty {
        print("String array is empty. Adding default value...")
        stringArray.append("Default")
    }
    if intArray.isEmpty {
        print("Int array is empty. Adding default value...")
        intArray.append(0)
    }
    print(stringArray)
    print(intArray)
}

// Создаем пустые массивы
var emptyStringArray: [String] = []
var emptyIntArray: [Int] = []

// Проверяем массивы на пустоту и выводим их в консоль
checkAndPrintArrays(&emptyStringArray, &emptyIntArray)


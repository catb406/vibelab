// Создаем массив элементов 'кол-во дней в месяцах'
let daysInMonths = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

// Создаем массив элементов 'название месяцов'
let monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

// Используя цикл for и массив 'кол-во дней в месяцах' выведем количество дней в каждом месяце (без имен месяцев)
for days in daysInMonths {
    print(days)
}

// Используем еще один массив с именами месяцев, чтобы вывести название месяца + количество дней
for i in 0..<monthNames.count {
    print("\(monthNames[i]): \(daysInMonths[i])")
}

// Сделаем то же самое, но используя массив tuples (кортежей) с параметрами (имя месяца, кол-во дней)
let monthsAndDays = zip(monthNames, daysInMonths)
for (month, days) in monthsAndDays {
    print("\(month): \(days)")
}

// Сделаем то же самое, но выводим дни в обратном порядке
for (month, days) in monthsAndDays.reversed() {
    print("\(month): \(days)")
}

// Для произвольно выбранной даты (месяц и день) посчитаем количество дней до этой даты от начала года
let month = 3 // март
let day = 27 // 27 марта
var daysCount = 0
for i in 0..<month {
    daysCount += daysInMonths[i]
}
daysCount += day
print("Дней с начала года до \(day).\(month): \(daysCount)")

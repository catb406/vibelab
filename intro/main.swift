//
//  main.swift
//  test
//
//  Created by e.bagryantseva on 16.03.2023.
//

import Foundation


func printMyName() {
    let myName = "Катя"
    let myAge = 22
    print("Меня зовут \(myName), мне \(myAge) года")
}


printMyName()


// Описание структур MyCar и Truck
struct MyCar: Hashable {
    let brand: String
    let year: Int
    let trunkVolume: Double
    var engineRunning: Bool
    var windowsOpen: Bool
    var filledTrunkVolume: Double
}

struct Truck: Hashable {
    let brand: String
    let year: Int
    let cargoVolume: Double
    var engineRunning: Bool
    var windowsOpen: Bool
    var filledCargoVolume: Double
}

enum CarAction {
    case startEngine
    case stopEngine
    case openWindows
    case closeWindows
    case loadCargo(volume: Double)
    case unloadCargo(volume: Double)
}

extension MyCar {
    mutating func perform(action: CarAction) {
        switch action {
        case .startEngine:
            engineRunning = true
        case .stopEngine:
            engineRunning = false
        case .openWindows:
            windowsOpen = true
        case .closeWindows:
            windowsOpen = false
        case .loadCargo(let volume):
            let availableVolume = trunkVolume - filledTrunkVolume
            let loadingVolume = min(availableVolume, volume)
            filledTrunkVolume += loadingVolume
        case .unloadCargo(let volume):
            let unloadingVolume = min(filledTrunkVolume, volume)
            filledTrunkVolume -= unloadingVolume
        }
    }
}

extension Truck {
    mutating func perform(action: CarAction) {
        switch action {
        case .startEngine:
            engineRunning = true
        case .stopEngine:
            engineRunning = false
        case .openWindows:
            windowsOpen = true
        case .closeWindows:
            windowsOpen = false
        case .loadCargo(let volume):
            let availableVolume = cargoVolume - filledCargoVolume
            let loadingVolume = min(availableVolume, volume)
            filledCargoVolume += loadingVolume
        case .unloadCargo(let volume):
            let unloadingVolume = min(filledCargoVolume, volume)
            filledCargoVolume -= unloadingVolume
        }
    }
}


var car1 = MyCar(brand: "Toyota", year: 2018, trunkVolume: 500, engineRunning: false, windowsOpen: false, filledTrunkVolume: 200)
var truck1 = Truck(brand: "Volvo", year: 2015, cargoVolume: 10000, engineRunning: false, windowsOpen: false, filledCargoVolume: 3000)

car1.perform(action: .startEngine)
car1.perform(action: .loadCargo(volume: 100))

truck1.perform(action: .startEngine)
truck1.perform(action: .unloadCargo(volume: 500))

var dict: [AnyHashable: String] = [:]
dict[AnyHashable(car1)] = "Car1"
dict[AnyHashable(truck1)] = "Truck1"

// Capture List - это список переменных или констант, которые используются для явного указания способа захвата значения замыкания. Захват значения означает сохранение копии значения в замыкании. В Swift замыкания могут захватывать значения по ссылке или по значению

class Person {
    let name: String

    init(name: String) {
        self.name = name
    }

    deinit {
        print("\(name) is being deinitialized")
    }
}

var person: Person? = Person(name: "John")
var greet: (() -> Void)?

greet = { [weak person] in
    if let person = person {
        print("Hello, \(person.name)!")
    } else {
        print("Hello, stranger!")
    }
}

person = nil
greet?()

// у нас есть класс Person с именем и инициализатором. Мы создаем экземпляр Person и замыкание greet. В Capture List замыкания greet мы указываем [weak person], что означает слабый захват переменной person. Это позволяет избежать сильной циклической ссылки между замыканием и объектом Person.

// Когда мы устанавливаем person = nil, объект Person освобождается, и затем мы вызываем замыкание greet. Захват слабой ссылки позволяет нам избежать утечки памяти, и замыкание корректно выводит "Hello, stranger!", поскольку объект Person был освобожден.

import UIKit

class Car {
    weak var driver: Man?

    deinit {
        print("машина удалена из памяти")
    }
}

class Man {
    var myCar: Car?

    deinit {
        print("мужчина удален из памяти")
    }
}

var car: Car? = Car()
var man: Man? = Man()

car?.driver = man
man?.myCar = car

car = nil
man = nil

// Проблема в этом коде заключается в том, что между двумя классами Car и Man возникает сильная циклическая ссылка. Объекты Car и Man удерживают сильные ссылки друг на друга через свойства driver и myCar. В результате, когда мы пытаемся освободить объекты, установив car = nil и man = nil, они не освобождаются, потому что сильные ссылки между ними остаются.
// Для решения этой проблемы, можно использовать слабые ссылки


protocol ManWithPassport: AnyObject {
    func clearPassport()
}

class NewMan: ManWithPassport {
    var passport: Passport? {
        didSet {
            passport?.ownerDelegate = self
        }
    }
    
    func clearPassport() {
        passport = nil
    }
    
    deinit {
        print("мужчина удален из памяти")
    }
}

class Passport {
    weak var ownerDelegate: ManWithPassport?
    let man: NewMan
    
    init(man: NewMan) {
        self.man = man
    }
    
    deinit {
        print("паспорт удален из памяти")
    }
}

var man2: NewMan? = NewMan()
var passport: Passport? = Passport(man: man2!)
man2?.passport = passport
man2?.clearPassport()
passport = nil
man2 = nil

// Создайте по 2 enum с разным типом RawValue

enum Weekday: String {
    case monday = "Понедельник"
    case tuesday = "Вторник"
    case wednesday = "Среда"
    case thursday = "Четверг"
    case friday = "Пятница"
    case saturday = "Суббота"
    case sunday = "Воскресенье"
}

enum Month: Int {
    case january = 1
    case february = 2
    case march = 3
    case april = 4
    case may = 5
    case june = 6
    case july = 7
    case august = 8
    case september = 9
    case october = 10
    case november = 11
    case december = 12
}

// Создайте несколько enum для заполнения полей стркутуры - анкета сотрудника: enum пол, enum категория возраста, enum стаж

enum Gender {
    case male
    case female
}

enum AgeCategory {
    case child
    case teenager
    case adult
    case elderly
}

enum WorkExperience {
    case lessThanOneYear
    case oneToFiveYears
    case fiveToTenYears
    case moreThanTenYears
}

// Создать enum со всеми цветами радуги
enum RainbowColor: String {
    case red
    case orange
    case yellow
    case green
    case blue
    case indigo
    case violet
}

//  Создать функцию, которая содержит массив разных case'ов enum и выводит содержимое в консоль. Пример результата в консоли 'apple green', 'sun red' и т.д.
func printEnumCases() {
    print("apple \(RainbowColor.green.rawValue)")
    print("sun \(RainbowColor.red.rawValue)")
    print("sky \(RainbowColor.blue.rawValue)")
    print("grass \(RainbowColor.green.rawValue)")
}

printEnumCases()

//Создать функцию, которая выставляет оценки ученикам в школе, на входе принимает значение enum Score: String {<Допишите case'ы} и выводит числовое значение оценки
enum Score: String {
    case a = "Отлично"
    case b = "Хорошо"
    case c = "Удовлетворительно"
    case d = "Неудовлетворительно"
}

func printScoreValue(score: Score) {
    switch score {
    case .a:
        print("5")
    case .b:
        print("4")
    case .c:
        print("3")
    case .d:
        print("2")
    }
}

printScoreValue(score: .a)
// Создать метод, которая выводит в консоль какие автомобили стоят в гараже, используйте enum
enum Car {
   case  sedan, suv, truck, sportsCar
}

func printGarageCars() {
        let cars: [Car] = [.sedan, .suv, .truck, .sportsCar]
        for car in cars {
            print("There is a \(car) in the garage.")
        }
    }
printGarageCars()
